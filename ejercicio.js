function ruedaJuguete(diametro){
    if(diametro <= 10){
        console.log("Es una rueda para un juguete pequeño");
    }else if(diametro > 10 && diametro < 20){
        console.log("Es una rueda para un juguete mediano");
    }else if(diametro >= 20){
        console.log("Es una rueda para un juguete grande");
    }
}

ruedaJuguete(5);
ruedaJuguete(7);
ruedaJuguete(14);
ruedaJuguete(20);
ruedaJuguete(31);